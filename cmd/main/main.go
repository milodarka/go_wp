package main

import(
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"github.com/junzhu/gorm/dialects/mysql"
	"github.com/milodarka/go_wp/pkg/routes"
)

func main(){
	r := mux.NewRouter()
	routes.RegisterSomethingRoutes(r)
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe("localhost:9010"), r)
}