package model
import (
	"github.com/jinzhu/gorm"
	"github.com/milodarka/go_wp/config"
)

var db * gorm.db
type Something struct {
	gorm.Model
	Name string `gorm: ""json: "name"`
	Something string `json: "something"`
	SomethingElse string `json: "somethingElse"`
}

func init(){
	config.Connect()
	db = config.GetDB()
	db.AutoMigrate(&Something{})
}

func (s *Something) CreateSomething() *Something {
	db.NewRecord(s)
	db.Create(&s)
	return s
}

func GetAllSomething() []Something{
	var Something []Something
	db.Find(&Something)
	return Something
}

func GetSomethingById(Id int64) (*Something, *gorm.DB){
	var getSomething Something
	db := db.Where("ID=?", Id).Find(&getSomething)
	return &getSomething, db
}

func DeleteSomething(Id int64) Something{
	var something Something
	db.Where("ID=?", Id).Delete(something)
	return something
}