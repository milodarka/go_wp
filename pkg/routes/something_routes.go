package routes

import (
	"github.com/gorilla/mux "
	"github.com/milodarka/go_wp/pkg/contollers"
)
var RegisterSomethingRoutes = func(router *mux.Router){
	router.HandleFunc("/something/", controllers.Create).Methods("POST")
	router.HandleFunc("/something/",controllers. GetSomething).Methods("GET")
	router.HandleFunc("/something/{somethingID}", contollers.GetSomethingByID).Methods("GET")
	router.HandleFunc("/something/{somethingiID", contollers.UpdateSomething).Methods("PUT")
	router.HandleFunc("/something/{somethingidID}", contollers.DeleteSomething).Methods("DELETE")
}
func main(){

}