package contollers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
	"github.com/milodarka/go_wp/pkg/utils"
	"github.com/milodarka/go_wp/pkg/models"
	

)

var NewSomething model.Something

func GetSomething(w http.ResponseWriter, r *http.Request){
	newSomething := models.GetAllSomething()
	res, _ := json.Marshal(newSomething)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func GetSomethingByID(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	somethingID = vars["somethingID"]
	ID, err = strconv.ParseInt(somethingID, 0, 0)

	if err != nil {
		fmt.Println("parsing error")
	}

	somethingDetails, _ := models.GetSomethingByID(ID)
	res, _ := json.Marshal(somethingDetails)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func CreateSomething(w http.ResponseWriter, r *http.Request){
	CreateSomething := &models.Something{}
	utils.ParseBody(r, CreateSomething)
	s := CreateSomething.CreateSomething()
	res, _ := json.Marshal(s)
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func DeleteSomething(w, http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	somethingID := vars["somethingID"]
	ID, err  strconv.ParseInt(somethingID, 0,0)
	if err != nil {
		fmt.Println("error while parsing and deleting")
	}
	something := models.DeleteSomething(ID)
	res, _ := json.Marshal(something)
	w.WriteHeader(http.StatusOK)
	w.Write(res)


}

func UpdateSomething(w, http.ResponseWriter, r *http.Request){
	var updateSomething := &models.Something{}
	utils.ParseBody(r, updateSomething)
	vars := mux.Vars(r)
	somethingID := vars["somethingID"]
	ID, err := strconv.ParseInt(somethingID, 0, 0)
	if err != nil {
		fmt.Println("error while parsing")
	}
	somethingDetails, db := models.GetSomethingByID(ID)
	if updateSomething.Name != ""{
		somethingDetails.Name = updateSomething.Name
	}
	if updateSomething.Something != ""{
		somethingDetails.Something = updateSomething.Something
	}
	if updateSomething.SomethingElse != ""{
		somethingDetails.SomethingElse = updateSomething.SomethingElse
	}
	db.Save(&somethingDetails)
	w.Header().Set("Content-Type", "pkglication/json")
	res, _ := json.Marshal(somethingDetails)
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}